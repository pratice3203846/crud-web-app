<?php
// Enable error reporting
error_reporting(E_ALL);
ini_set('display_errors', 1);

class User
{
    private $pdo;

    function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    // Create a new user with the same password as typed in the form.
    public function createUser($username, $email, $password)
    {
        // Prepare and execute the SQL statement
        $stmt = $this->pdo->prepare("INSERT INTO users (username, email, password) VALUES (?, ?, ?)");
        $stmt->execute([
            $username, $email, $password
        ]);

        return true; // Return true on success
    }
    // Create a new user with password being hashed
    // public function createUser($username, $email, $password)
    // {
    //     $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    //     try {
    //         $stmt = $this->pdo->prepare("INSERT INTO users (username, email, password) VALUES (?, ?, ?)");
    //         $stmt->execute([$username, $email, $hashedPassword]);
    //         return true;
    //     } catch (PDOException $e) {
    //         // Handle exception, log error, etc.
    //         return false;
    //     }
    // }

    // Update an existing User object
    public function updateUser($userId, $username, $email, $password)
    {
        try {
            // Prepare the SQL statement
            if (!empty($password)) {
                // If a new password is provided, update user information including the password
                $stmt = $this->pdo->prepare("UPDATE users SET username=?, email=?, password=? WHERE id=?");
                $stmt->execute([$username, $email, $password, $userId]);
            } else {
                // If no new password is provided, update user information excluding the password
                $stmt = $this->pdo->prepare("UPDATE users SET username=?, email=? WHERE id=?");
                $stmt->execute([$username, $email, $userId]);
            }

            return true; // Return true on success
        } catch (PDOException $e) {
            // Handle any exceptions if the query fails
            // You might want to log the error or handle it differently
            return false;
        }

        $stmt->close();
    }


    // Delete an existing user
    public function deleteUser($userId)
    {
        try {
            $stmt = $this->pdo->prepare("DELETE FROM users WHERE id = ?");
            $stmt->execute([$userId]);
            return true;
        } catch (PDOException $e) {
            // Handle exception, log error, etc.
            return false;
        }
    }

    // Get user details by ID
    public function getUserById($userId)
    {
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM users WHERE id = ?");
            $stmt->execute([$userId]);
            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            return $user;
        } catch (PDOException $e) {
            // Handle exception, log error, etc.
            return false;
        }
    }

    // Get all users
    public function getUsers($searchKeyword = null)
    {
        try {
            $query = "SELECT * FROM users";
            if (!empty($searchKeyword)) {
                $query .= " WHERE username LIKE '%$searchKeyword%'";
            }
            $stmt = $this->pdo->query($query);
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $users;
        } catch (PDOException $e) {
            // Handle exception, log error, etc.
            return false;
        }
    }
}
