<?php
$hostname = "localhost";
$username = "root"; // MySQL username
$password = "";     // MySQL password 
$database = "crud";

try {
    $pdo = new PDO("mysql:host={$hostname};dbname={$database}", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Connection failed: " . $e->getMessage());
}
