<?php
// Enable error reporting
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("db.php");
require_once("user.php");

session_start();

if (isset($_GET['id'])) {
    $userId = $_GET['id'];
    $user = new User($pdo);

    if ($user->deleteUser($userId)) {
        $_SESSION['success_message'] = "User deleted successfully!";
    } else {
        $_SESSION['error_message'] = "Error deleting user.";
    }

    header("Location: index.php");
    exit;
} else {
    header("Location: index.php");
    exit;
}
