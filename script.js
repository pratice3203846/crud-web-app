// Search box
document.addEventListener("DOMContentLoaded", function () {
    // Search input element
    var searchInput = document.getElementById("searchInput");

    // Add event listener for input event
    searchInput.addEventListener("input", function () {
        var searchText = searchInput.value.toLowerCase();
        var rows = document.querySelectorAll("#userTableBody tr");

        // Loop through all table rows
        rows.forEach(function (row) {
            var username = row.querySelector(".username").textContent.toLowerCase();
            var email = row.querySelector(".email").textContent.toLowerCase();
            // Check if username or email contains the search text
            if (username.includes(searchText) || email.includes(searchText)) {
                row.style.display = ""; // Show the row
            } else {
                row.style.display = "none"; // Hide the row
            }
        });
    });
});
