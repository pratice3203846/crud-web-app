<?php
require_once("db.php");
require_once("user.php");

// Query to fetch users from the database
$stmt = $pdo->query("SELECT * FROM users");

// Fetch all users
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User List</title>
    <!-- Bootstrap CSS -->
    <style>
        /* Hide password column by default */
        .password-column {
            display: none;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <h1>User List</h1>
        <!-- Button to navigate to createUser.php -->
        <a href="createUser.php" class="btn btn-primary mb-3">Create User</a>

        <!-- Search Box -->
        <div class="mb-3">
            <input type="text" id="searchInput" class="form-control" placeholder="Search by Username">
        </div>

        <!-- Table to display users -->
        <table class="table table-striped table-hover">
            <!-- Table header -->
            <thead>
                <tr>
                    <th><a>ID</a></th>
                    <th><a>Username</a></th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Action</th>
                </tr>
            </thead>
            <!-- Table body -->
            <tbody id="userTableBody">
                <!-- PHP code for fetching and displaying users -->
                <?php
                if (count($users) > 0) {
                    foreach ($users as $user) {
                        echo '<tr>';
                        echo '<td>' . $user['id'] . '</td>';
                        echo '<td class="username">' . $user['username'] . '</td>';
                        echo '<td class="email">' . $user['email'] . '</td>';
                        echo '<td class="password-column">' . $user['password'] . '</td>';
                        echo '<td><a>*******************************</a></td>';
                        echo '<td>';
                        // Show button links to showUser.php with user ID
                        echo ' <a href="showUser.php?id=' . $user['id'] . '" class="btn btn-info btn-sm">Show</a>';
                        // Edit button links to editUser.php with user ID
                        echo '<a href="editUser.php?id=' . $user['id'] . '" class="btn btn-warning btn-sm">Edit</a>';
                        // Delete button links to deleteUser.php with user ID
                        echo ' <a href="deleteUser.php?id=' . $user['id'] . '" class="btn btn-danger btn-sm">Delete</a>';
                        echo '</td>';
                        echo '</tr>';
                    }
                } else {
                    // Display message if no users found
                    echo '<tr><td colspan="5">No users found.</td></tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- Bootstrap JS Bundle (Popper included) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Search functionality -->
    <script src="script.js"></script>
</body>

</html>