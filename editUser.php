<?php
// Enable error reporting
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("db.php");
require_once("user.php");

// Initialize session 
session_start();

// Check if user ID is provided in the URL
if (isset($_GET['id'])) {
    // Fetch user details from the database based on the provided ID
    $userId = $_GET['id'];
    $user = new User($pdo);
    $userDetails = $user->getUserById($userId);

    // Check if user with the provided ID exists
    if ($userDetails) {
        // Process form submission if form is submitted
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Retrieve form data
            $username = $_POST["username"];
            $email = $_POST["email"];
            $password = $_POST["password"];

            // Update user details
            if ($user->updateUser($userId, $username, $email, $password)) {
                // Set a session variable to hold success message
                $_SESSION['success_message'] = "User details updated successfully!";
                // Redirect to the User Information page with the user ID
                header("Location: showUser.php?id=" . $userId);
                // Terminate script execution after redirection
                exit;
            } else {
                // Handle error if update fails
                $_SESSION['error_message'] = "Error updating user.";
                // Redirect back to the edit page with the same user ID
                header("Location: editUser.php?id=" . $userId);
                // Terminate script execution after redirection
                exit;
            }
        }
    } else {
        // Redirect to user list page if user with the provided ID is not found
        $_SESSION['error_message'] = "User not found.";
        header("Location: index.php");
        // Terminate script execution after redirection
        exit;
    }
} else {
    // Redirect to user list page if ID is not provided
    header("Location: index.php");
    // Terminate script execution after redirection
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit User</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <h1>Edit User</h1>
        <?php if (isset($_SESSION['error_message'])) : ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $_SESSION['error_message']; ?>
            </div>
            <?php unset($_SESSION['error_message']); ?>
            <!-- Unset the session variable to clear the message -->
        <?php endif; ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?id=" . $userId); ?>" method="post">
            <div class="mb-3">
                <label for="username" class="form-label">Username:</label>
                <input type="text" class="form-control" name="username" value="<?php echo $userDetails['username']; ?>" required>
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email:</label>
                <input type="email" class="form-control" name="email" value="<?php echo $userDetails['email']; ?>" required>
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password:</label>
                <input type="password" class="form-control" name="password" required>
            </div>
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </form>
        <!-- Button to return to user list -->
        <a href="index.php" class="btn btn-secondary mt-3">Return to User List</a>
    </div>
    <!-- Bootstrap JS Bundle (Popper included) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>