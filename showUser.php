<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Information</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <?php
        session_start();

        // Check if success message is set in the session
        if (isset($_SESSION['success_message'])) {
            echo '<div class="alert alert-success" role="alert">';
            echo $_SESSION['success_message'];
            echo '</div>';
            // Unset the session variable to clear the message
            unset($_SESSION['success_message']);
        }

        require_once("db.php");
        require_once("user.php");

        // Check if user ID is provided in the URL
        if (isset($_GET['id'])) {
            // Fetch user details from the database based on the provided ID
            $userId = $_GET['id'];
            $user = new User($pdo);
            $userDetails = $user->getUserById($userId);

            // Check if user with the provided ID exists
            if ($userDetails) {
                // Display user information in a table
                echo "<h1>User Information</h1>";
                echo "<table class='table table-striped table-hover'>";
                echo "<tr><th>ID</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Password</th>
                      </tr>";
                echo "<tr>";
                echo "<td>" . $userDetails['id'] . "</td>";
                echo "<td>" . $userDetails['username'] . "</td>";
                echo "<td>" . $userDetails['email'] . "</td>";
                echo "<td>" . $userDetails['password'] . "</td>";
                echo "</tr>";
                echo "</table>";

                echo '<a href="index.php" class="btn btn-secondary mt-3">Return to User List</a>';
            } else {
                // If user with the provided ID is not found, display an error message
                echo "<div class='alert alert-danger' role='alert'>User not found.</div>";
            }
        } else {
            // If ID is not provided in the URL, redirect to the User List page
            header("Location: index.php");
            // Terminate script execution after redirection
            exit;
        }
        ?>
    </div>
    <!-- Bootstrap JS Bundle (Popper included) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>